#include <QDebug>

#include "controldirection.h"

ControlDirection::ControlDirection(QObject *parent) : QObject(parent)
{
    SOCKET_STATE res;
    this->m_sock = new SocketClient("140.112.14.229", 9090);
    qDebug() << "Connect to: " << "140.112.14.229:9090";
    res = this->m_sock->initialize();
    if (res != SUCCESS) {
        qDebug() << "FAILED Initialized";
        return;
    }
    res = this->m_sock->connectToRemote();
    if (res != SUCCESS) {
        qDebug() << "Failed Connection";
        return;
    }
}


void ControlDirection::sendPress(QString direction) {
    SOCKET_STATE res;
    qDebug() << direction;
    res = this->m_sock->sendData(direction.toStdString().c_str());
    if (res != SUCCESS) {
        qDebug() << "Failed Send";
        return;
    }
}
