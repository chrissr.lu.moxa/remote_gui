import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0

Item {

    property string what_clicked: "Camera"
    property var model: [
         {image_source: "img/camera.svg", image_text: "Camera", index: 0},
         {image_source: "img/eye.svg", image_text: "Person & Face", index: 1},
         {image_source: "img/swimmer.svg", image_text: "Action & Group", index: 2},
    ]

    Column {
        anchors.fill: parent
        spacing: 30

        Row {
            id: row0

            Button {
                id: button0
                Layout.fillWidth: true
                flat: true
                Layout.alignment: Qt.AlignCenter
                hoverEnabled: false
                Image {
                    id: image0
                    source: model[0].image_source
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    sourceSize.width: 30
                    sourceSize.height: 30
                }
                ColorOverlay {
                    id: overlayImage0
                    anchors.fill: image0
                    source: image0
                    color: what_clicked === model[0].image_text ? "cyan": "white"
                }
                MouseArea {
                    id: mouseArea_image0
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        what_clicked = model[0].image_text
                        if (what_clicked === model[0].image_text) {
                            name0.color = "cyan"
                            overlayImage0.color = "cyan"
                            overlayImage1.color = "white"; name1.color = "white"
                            overlayImage2.color = "white"; name2.color = "white"
                        } else {
                            name0.color = "white"
                            overlayImage0.color = "white"
                        }
                    }
                    onEntered: {
                        if (what_clicked !== model[0].image_text) {
                            name0.color = "#b3e5fc";
                            overlayImage0.color = "#b3e5fc";
                        }

                    }
                    onExited: {
                        if (what_clicked !== model[0].image_text) {
                            name0.color = "white";
                            overlayImage0.color = "white";
                        }
                    }
                }
            }

            Text {
                id: name0
                anchors.verticalCenter: parent.verticalCenter
                text: model[0].image_text
                font.family: "hack"
                font.pointSize: 12
                color: what_clicked === model[0].image_text ? "cyan": "white"
                MouseArea {
                    id: mouseArea_text0
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        what_clicked = model[0].image_text
                        if (what_clicked === model[0].image_text) {
                            name0.color = "cyan"
                            overlayImage0.color = "cyan"
                            overlayImage1.color = "white"; name1.color = "white"
                            overlayImage2.color = "white"; name2.color = "white"
                        } else {
                            name0.color = "white"
                            overlayImage0.color = "white"
                        }

                    }
                    onEntered: {
                        if (what_clicked !== model[0].image_text) {
                            name0.color = "#b3e5fc";
                            overlayImage0.color = "#b3e5fc";
                        }
                    }
                    onExited: {
                        if (what_clicked !== model[0].image_text) {
                            name0.color = "white";
                            overlayImage0.color = "white";
                        }
                    }
                }
            }
        }

        Row {
            id: row1

            Button {
                id: button1
                Layout.fillWidth: true
                flat: true
                Layout.alignment: Qt.AlignCenter
                hoverEnabled: false
                Image {
                    id: image1
                    source: model[1].image_source
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    sourceSize.width: 30
                    sourceSize.height: 30
                }
                ColorOverlay {
                    id: overlayImage1
                    anchors.fill: image1
                    source: image1
                    color: what_clicked === model[1].image_text ? "cyan": "white"
                }
                MouseArea {
                    id: mouseArea_image1
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        what_clicked = model[1].image_text
                        if (what_clicked === model[1].image_text) {
                            name1.color = "cyan"
                            overlayImage1.color = "cyan"
                            overlayImage0.color = "white"; name0.color = "white"
                            overlayImage2.color = "white"; name2.color = "white"
                        } else {
                            name1.color = "white"
                            overlayImage1.color = "white"
                        }
                    }
                    onEntered: {
                        if (what_clicked !== model[1].image_text) {
                            name1.color = "#b3e5fc";
                            overlayImage1.color = "#b3e5fc";
                        }

                    }
                    onExited: {
                        if (what_clicked !== model[1].image_text) {
                            name1.color = "white";
                            overlayImage1.color = "white";
                        }
                    }
                }
            }

            Text {
                id: name1
                anchors.verticalCenter: parent.verticalCenter
                text: model[1].image_text
                font.family: "hack"
                font.pointSize: 12
                color: what_clicked === model[1].image_text ? "cyan": "white"
                MouseArea {
                    id: mouseArea_text1
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        what_clicked = model[1].image_text
                        if (what_clicked === model[1].image_text) {
                            name1.color = "cyan"
                            overlayImage1.color = "cyan"
                            overlayImage0.color = "white"; name0.color = "white"
                            overlayImage2.color = "white"; name2.color = "white"
                        } else {
                            name.color = "white"
                            overlayImage1.color = "white"
                        }

                    }
                    onEntered: {
                        if (what_clicked !== model[1].image_text) {
                            name1.color = "#b3e5fc";
                            overlayImage1.color = "#b3e5fc";
                        }
                    }
                    onExited: {
                        if (what_clicked !== model[1].image_text) {
                            name1.color = "white";
                            overlayImage1.color = "white";
                        }
                    }
                }
            }
        }
        Row {
            id: row2

            Button {
                id: button2
                Layout.fillWidth: true
                flat: true
                Layout.alignment: Qt.AlignCenter
                hoverEnabled: false
                Image {
                    id: image2
                    source: model[2].image_source
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    sourceSize.width: 30
                    sourceSize.height: 30
                }
                ColorOverlay {
                    id: overlayImage2
                    anchors.fill: image2
                    source: image2
                    color: what_clicked === model[2].image_text ? "cyan": "white"
                }
                MouseArea {
                    id: mouseArea_image2
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        what_clicked = model[2].image_text
                        if (what_clicked === model[2].image_text) {
                            name2.color = "cyan"
                            overlayImage2.color = "cyan"
                            overlayImage0.color = "white"; name0.color = "white"
                            overlayImage1.color = "white"; name1.color = "white"
                        } else {
                            name2.color = "white"
                            overlayImage2.color = "white"
                        }
                    }
                    onEntered: {
                        if (what_clicked !== model[2].image_text) {
                            name2.color = "#b3e5fc";
                            overlayImage2.color = "#b3e5fc";
                        }

                    }
                    onExited: {
                        if (what_clicked !== model[2].image_text) {
                            name2.color = "white";
                            overlayImage2.color = "white";
                        }
                    }
                }
            }

            Text {
                id: name2
                anchors.verticalCenter: parent.verticalCenter
                text: model[2].image_text
                font.family: "hack"
                font.pointSize: 12
                color: what_clicked === model[2].image_text ? "cyan": "white"
                MouseArea {
                    id: mouseArea_text2
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        what_clicked = model[2].image_text
                        if (what_clicked === model[2].image_text) {
                            name2.color = "cyan"
                            overlayImage2.color = "cyan"
                            overlayImage0.color = "white"; name0.color = "white"
                            overlayImage1.color = "white"; name1.color = "white"
                        } else {
                            name2.color = "white"
                            overlayImage2.color = "white"
                        }

                    }
                    onEntered: {
                        if (what_clicked !== model[2].image_text) {
                            name2.color = "#b3e5fc";
                            overlayImage2.color = "#b3e5fc";
                        }
                    }
                    onExited: {
                        if (what_clicked !== model[2].image_text) {
                            name2.color = "white";
                            overlayImage2.color = "white";
                        }
                    }
                }
            }
        }
    }
    /*

    ColumnLayout {
        anchors.top: parent.top
        anchors.topMargin: 120
        anchors.left: parent.left
        anchors.leftMargin: 80
        spacing: 30


        Repeater {
            id: repeater
            model: [
             {image_source: "img/camera.svg", image_text: "Camera", index: 0},
             {image_source: "img/eye.svg", image_text: "Head & Eye", index: 1},
             {image_source: "img/swimmer.svg", image_text: "Action", index: 2},
             {image_source: "img/users.svg", image_text: "Group", index: 3},
            ]

            Row {
                id: row

                Button {
                    id: buttons
                    Layout.fillWidth: true
                    flat: true
                    Layout.alignment: Qt.AlignCenter
                    hoverEnabled: false
                    Image {
                        id: image
                        source: modelData.image_source
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        sourceSize.width: 30
                        sourceSize.height: 30
                    }
                    ColorOverlay {
                        id: overlayImage
                        anchors.fill: image
                        source: image
                        // color: what_clicked === modelData.image_text ? "cyan": "white"
                        color: modelData.color
                    }
                    MouseArea {
                        id: mouseArea_image
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: {
                            what_clicked = modelData.image_text
                            if (what_clicked === modelData.image_text) {
                                name.color = "cyan"
                                overlayImage.color = "cyan"
                            } else {
                                name.color = "white"
                                overlayImage.color = "white"
                            }
                        }
                        onEntered: {
                            if (what_clicked !== modelData.image_text) {
                                name.color = "#b3e5fc";
                                overlayImage.color = "#b3e5fc";
                            }

                        }
                        onExited: {
                            if (what_clicked !== modelData.image_text) {
                                name.color = "white";
                                overlayImage.color = "white";
                            }
                        }
                    }
                }

                Text {
                    id: name
                    anchors.verticalCenter: parent.verticalCenter
                    text: modelData.image_text
                    font.family: "hack"
                    font.pointSize: 12
                    color: what_clicked === modelData.image_text ? "cyan": "white"
                    MouseArea {
                        id: mouseArea_text
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: {
                            what_clicked = modelData.image_text
                            if (what_clicked === modelData.image_text) {
                                name.color = "cyan"
                                overlayImage.color = "cyan"
                            } else {
                                name.color = "white"
                                overlayImage.color = "white"
                            }

                        }
                        onEntered: {
                            if (what_clicked !== modelData.image_text) {
                                name.color = "#b3e5fc";
                                overlayImage.color = "#b3e5fc";
                            }
                        }
                        onExited: {
                            if (what_clicked !== modelData.image_text) {
                                name.color = "white";
                                overlayImage.color = "white";
                            }
                        }
                    }
                }
            }
        }
    }
    */
}
