#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "showimage.h"
#include "controldirection.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qSetMessagePattern("%{time h:mm:ss.zzz}: %{file}(%{line}): %{message}");

    QGuiApplication app(argc, argv);

    qmlRegisterType<ShowImage>("showimage", 1, 0, "ShowImage");
    qmlRegisterType<ControlDirection>("controldirection", 1, 0, "ControlDirection");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
