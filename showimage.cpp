#include "showimage.h"

#include <chrono>
#include <QtConcurrent>
#include <QtCore>
#include <QTime>


ShowImage::ShowImage(QQuickItem *parent) : QQuickPaintedItem(parent)
{
    QString path = ":/img/cat.jpg";
    this->m_image = QImage(path);
    this->setRenderTarget(QQuickPaintedItem::FramebufferObject);
    this->m_frame_count = 0;
}

void ShowImage::paint(QPainter *painter)
{
    QRectF bounding_rect = boundingRect();
    QImage scaled = this->m_image.scaledToHeight(bounding_rect.height());
    QPointF center = bounding_rect.center() - scaled.rect().center();

    if(center.x() < 0)
        center.setX(0);
    if(center.y() < 0)
        center.setY(0);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->setRenderHint(QPainter::HighQualityAntialiasing);
    painter->setRenderHint(QPainter::SmoothPixmapTransform);
    painter->drawImage(center, scaled);
}

void ShowImage::update_image()
{
    update();
}

void ShowImage::setImage(const QImage &image)
{
    this->m_image = image.copy();
    emit this->imageChanged();
}

void ShowImage::initialize()
{
    SOCKET_STATE res;
    qDebug() << "init";
    this->m_sock = new SocketClient(this->m_ip.toStdString(), this->m_port);
    res = this->m_sock->initialize();
    if (res != SUCCESS) {
        qDebug() << "FAILED Initialized";
        return;
    }

}

void ShowImage::connect()
{
    qDebug() << "connect";
    SOCKET_STATE res;
    qDebug() << "ip: " + this->m_ip;
    qDebug() << "port: " + QString::number(this->m_port);
    res = this->m_sock->connectToRemote();
    if (res != SUCCESS) {
        qDebug() << "FAILED Connection";
        return;
    }
    auto thread = QtConcurrent::run(this, &ShowImage::start);
}

void ShowImage::start()
{
    QImage image(640, 480, QImage::Format_RGB888);
    SOCKET_STATE res;
    char return_value[1024] = {0};
    this->m_time.start();
    this->m_frame_count = 0;
    QString current_request = "";
    double current_fps = 0.0;

    while(true) {
        // Check timer
        if(current_request != this->m_request) {
            current_request = this->m_request;
            this->m_frame_count = 0;
            this->m_time.restart();
        } else {
            this->m_frame_count ++;
        }

        // Request picture
        memset(this->image_buffer, 0, DEFAULT_IMAGE_SIZE);
        memset(&return_value, 0, 1024);
        this->image_size = 0;

        res = this->m_sock->recvData(this->image_buffer,
                                     this->image_size,
                                     this->m_request.toStdString(),
                                     return_value);
        if (res != SUCCESS) {
            qDebug() << "FAILED Recv";
            break;
        }
        image = QImage::fromData((uchar*)this->image_buffer, this->image_size, "jpg");
        this->setImage(image);

        QString msg = QString::fromUtf8(return_value);
        if (msg != "end") {
            this->set_text(msg);
        } else {
            this->set_text("");
        }

        current_fps = this->m_frame_count / (float(m_time.elapsed()) / 1000.0f);
        this->set_fps(QString::number(current_fps, 'd', 1));
    }
    memset(this->image_buffer, 0, DEFAULT_IMAGE_SIZE);
    this->image_size = 0;
}

void ShowImage::set_fps(QString fps)
{
    if (fps == this->m_fps) {
        return;
    }
    this->m_fps = fps;
    emit this->fpsChanged(this->m_fps);
}


void ShowImage::set_text(const QString &text)
{
    this->m_text = text;
    emit this->textChanged(text);
}

void ShowImage::disconnect()
{
    this->m_sock->disconnect();
    delete this->m_sock;
}
