import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.12

import controldirection 1.0

Window {
    id: applicationWindow
    color: "#242526" // Already display by opengl
    width: Screen.width
    height: Screen.height
    // visibility: "FullScreen"
    title: qsTr("Robotics Laboratory UI")
    visible: true
    Material.theme: Material.Dark
    Material.accent: Material.Teal

    property string display_handler: "Camera"

    Sidebar {
        id: sidebar
        // anchors.fill: parent
        width: parent.width*0.15
        height: parent.height*0.5
        anchors.top: parent.top
        anchors.topMargin: 140
        anchors.left: parent.left
        anchors.leftMargin: 50
        onWhat_clickedChanged: {
            display_handler = what_clicked;
        }
    }

    Direction {
        id: direction
        width: parent.width*0.15
        height: parent.height*0.1
        anchors.left: parent.left
        anchors.leftMargin: 100
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 120
    }

    /* vertical rule */
    Rectangle {
        id: vertical_rule
        width: 2
        height: parent.height*0.8
        color: "slategrey"
        opacity: 0.6
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.2
        anchors.verticalCenter: parent.verticalCenter
    }
    /* Main Windows */
    Display {
        id: display
        anchors.left: vertical_rule.right
        anchors.leftMargin: 40
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height*0.8
        width: parent.width*0.7
        current_display: display_handler
        onCurrent_displayChanged: {
            if (current_display === "Camera") {
                left_title = "RGB Camera"
                right_title = "Depth Camera"
            } else if (current_display === "Person & Face") {
                left_title = "YOLOv3"
                right_title = "Head & eye"
            } else if (current_display === "Action & Group") {
                left_title = "Engagement & Intention"
                right_title = "Group"
            }
        }
    }


    Item {
        width: parent.height*0.1
        height: parent.height*0.1
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10

        Image {
            anchors.fill: parent
            id: logo
            source: "img/Logo.svg"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            mipmap: true
        }
        ColorOverlay {
            id: overlaylogo
            anchors.fill: logo
            source: logo
            color: "lightsteelblue"
        }
    }
    /*
    Item {
        focus: true;
        Keys.enabled: true
        Keys.onEscapePressed: {
            Qt.quit();
        }

        Keys.onUpPressed: {
            direction.direction_button = "UP"
            control.sendPress("UP");
        }
        Keys.onDownPressed: {
            direction.direction_button = "DOWN"
            control.sendPress("DOWN");
        }
        Keys.onRightPressed: {
            direction.direction_button = "RIGHT"
            control.sendPress("RIGHT");
        }
        Keys.onLeftPressed: {
            direction.direction_button = "LEFT"
            control.sendPress("LEFT");
        }


        ControlDirection {
            id: control
        }
    }
    */

}
