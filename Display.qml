import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0

import showimage 1.0

Item {
    id: element
    property string current_display: "Camera"
    property int default_width: 1280
    property int default_height: 480
    property string left_fps: "0"
    property string right_fps: "0"
    property int left_port: 8000
    property int right_port: 8001
    property string ip: "192.168.0.220"

    property string left_title: "RGB Camera"
    property string right_title: "Depth Camera"

    property string left_info: ""
    property string right_info: ""

    width: default_width
    Row {
        spacing: 20

        ColumnLayout {
            Label {
                id: title_label_left
                text: left_title
                font.pointSize: 16
            }
            ShowImage {
                id: display1
                width: default_width/2
                height: default_height
                m_ip: ip
                m_port: left_port
                m_request: left_title
                Component.onCompleted: {
                    display1.initialize();
                    display1.connect();
                }
                onFpsChanged: {
                    left_fps = display1.m_fps;
                }
                onImageChanged: {
                    display1.update_image();
                }
                onTextChanged: {
                    left_info = display1.m_text;
                }
            }
            Column {
                Label {
                    text: "FPS: " + left_fps
                    styleColor: "#101011"
                    font.pointSize: 14
                }

                Label {
                    text: left_info
                    styleColor: "#101011"
                    font.pointSize: 14
                }
            }
        }

        ColumnLayout {
            Label {
                id: title_label_right
                text: right_title
                font.pointSize: 16
            }
            ShowImage {
                id: display2
                width: default_width/2
                height: default_height
                m_ip: ip
                m_port: right_port
                m_request: right_title
                Component.onCompleted: {
                    display2.initialize();
                    display2.connect();
                }
                onFpsChanged: {
                    right_fps = display2.m_fps;
                }
                onImageChanged: {
                    display2.update_image();
                }
                onTextChanged: {
                    right_info = display2.m_text;
                }
            }
            Column {
                Label {
                    text: "FPS: " + right_fps
                    styleColor: "#101011"
                    font.pointSize: 14
                }
                Label {
                    text: right_info
                    styleColor: "#101011"
                    font.pointSize: 14
                }
            }
        }
    }

    /*
    function stopDisplay() {
        console.log("stop");
        display1.disconnect();
        display2.disconnect();
        delay(2500, restart)
    }
    function restart() {
        console.log("restart");
        display1.initialize();
        display2.initialize();
        display1.connect();
        display2.connect();
    }

    Timer {
        id: timer
    }

    function delay(delayTime, cb) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.triggered.connect(cb);
        timer.triggered.connect(function release() {
            timer.triggered.disconnect(cb);
            timer.triggered.disconnect(release);
        })
        timer.start();
    }

    function delay_time(delayTime) {
        timer.interval = delayTime;
        timer.repeat = false;
        timer.start();
    }
    */
}
