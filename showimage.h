#ifndef SHOWIMAGE_H
#define SHOWIMAGE_H

#include <QObject>
#include <QQuickPaintedItem>
#include <QQuickItem>
#include <QPainter>
#include <QTime>

#include <mutex>

#include "socketclient.h"

#define DEFAULT_IMAGE_SIZE 4096*100


class ShowImage : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QImage m_image READ get_image WRITE setImage NOTIFY imageChanged)
    Q_PROPERTY(int m_port WRITE set_port)
    Q_PROPERTY(QString m_ip WRITE set_ip)
    Q_PROPERTY(QString m_request WRITE set_request)
    Q_PROPERTY(QString m_fps READ get_fps WRITE set_fps NOTIFY fpsChanged)
    Q_PROPERTY(QString m_text READ get_text WRITE set_text NOTIFY textChanged)
    // Q_PROPERTY(bool m_start WRITE start)

public:
    explicit ShowImage(QQuickItem *parent = nullptr);
    Q_INVOKABLE void initialize();
    Q_INVOKABLE void start();
    Q_INVOKABLE void connect();
    Q_INVOKABLE void disconnect();
    Q_INVOKABLE void update_image();

    void paint(QPainter *painter);
    void setImage(const QImage &image);
    void set_request(const QString &request) {this->m_request = request; this->m_frame_count = 0;}
    void set_port(const int &port) {this->m_port = port;}
    void set_ip(const QString &ip) {this->m_ip = ip;}
    void set_fps(QString fps);
    void set_text(const QString &text);


    QString get_fps() {return this->m_fps;}
    QImage get_image() {return this->m_image;}
    QString get_text() {return this->m_text;}
    // int port() const {return this->m_port;}
    // QString ip() const {return this->m_ip;}
    // bool start() const {return this->m_start;}

signals:
    void imageChanged();
    void fpsChanged(QString fps);
    void textChanged(QString text);

private:

    QString m_fps;
    SocketClient *m_sock;
    int m_port;
    QString m_ip;
    QString m_request;
    QString m_text;

    QImage m_image;
    char image_buffer[DEFAULT_IMAGE_SIZE];
    int image_size;

    QTime m_time;
    int m_frame_count;

};
#endif // SHOWIMAGE_H
