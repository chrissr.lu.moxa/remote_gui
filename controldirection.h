#ifndef CONTROLDIRECTION_H
#define CONTROLDIRECTION_H

#include <QObject>

#include "socketclient.h"

class ControlDirection : public QObject
{
    Q_OBJECT

public:
    explicit ControlDirection(QObject *parent = nullptr);

public slots:
    void sendPress(QString direction);

private:
    SocketClient *m_sock;
};

#endif // CONTROLDIRECTION_H
