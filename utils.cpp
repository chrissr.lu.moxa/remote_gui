#include "utils.h"

const QString path = ":/configuration/config.json";

QVariantMap cfgs;

void JsonParser() {

    QFile file(path);
    if(!file.exists()) {
        qDebug() << "Error: Cannot open file";
    }
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qFatal("Error: Device cannot open");
    }
    QString raw_data = file.readAll();
    file.close();

    QJsonParseError jsonError;
    QJsonDocument flowerJson = QJsonDocument::fromJson(raw_data.toUtf8(),&jsonError);
    if (jsonError.error != QJsonParseError::NoError){
        qDebug() << "Json error";
    }
    QJsonObject json_obj = flowerJson.object();
    cfgs = json_obj.toVariantMap();
}
